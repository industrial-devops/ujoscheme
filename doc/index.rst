.. UJO Schemes documentation master file

Welcome to UJO Schemes's documentation!
=======================================

API Documentation:

.. toctree::
   :glob:

   code-doc/*

UJO Schemes:

.. toctree::
   :glob:
   
   markdown/*
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
