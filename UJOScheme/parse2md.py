#
# Copyright (c) 2018-present, wobe-systems GmbH
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.
#

from string import Template
from lark.tree import Tree
from . import parser 


def linefeed2br(s):
    """Convert linefeeds to <br> tags.

    Markdown uses <br> tags for linebreaks in tables. This
    function allows to convert a text with linebreaks accordingly.
   
    Args:
        s (str): string to convert

    returns:
        str: a string with <br> tags for linefeeds
    """
    r = None
    for line in iter(s.splitlines()):
        if r is None:
            r = line.strip('\n ')
        else:
            r = r + "<br>" + line.strip('\n ')
    if r is None:
        r = ''
    return r


class ujsModuleProps():
    """ 
    this class contains a module description
    """
    # pylint: disable=too-few-public-methods
    def __init__(self):
        self.identifier = None
        self.doc = None

    def __str__(self):
        s = "# Module " + self.identifier
        if self.doc is not None:
            s = s + "\n\n" + self.doc + "\n"
        else:
            s = s + "\n"
        return s
                

class ujsConstraint():
    # pylint: disable=too-few-public-methods

    def __init__(self, owner=None):
        """
        attributes:
            owner_type : the class owning this constraints for building
            references
        """

        self.owner_type = ''
        self.item_name = None    # reference for items name
        self.owner = owner       # parser class reference


class ujsNotNull(ujsConstraint):
    # pylint: disable=too-few-public-methods
    def __init__(self, owner=None):
        ujsConstraint.__init__(self, owner)
        self.default_val = None

    def __str__(self):
        s = "not null"
        if self.default_val is not None:
            s = s + " default " + self.default_val
        return s


class ujsRangeValue():
    """
    representation of a value in a range constraint

    attributes:
        type : is a string representing the type of the constraint
                "equals", ...
    """
    def __init__(self, atype=None):
        self.type = atype
        self.value = None
        self.low_value = None
        self.high_value = None
        self.doc = ''

    def val_as_str(self):
        if self.type == 'equals':
            v = self.value
        elif self.type == 'above':
            v = ">= " + self.value
        elif self.type == 'below':
            v = "<= " + self.value
        elif self.type == 'word':
            v = self.value
        elif self.type == 'between':
            v = str(self.low_value) + ".." + str(self.high_value)

        return v

    def __str__(self):
        s = "| " + self.val_as_str() + ' | ' + self.doc + ' |'
        return s


class ujsLength(ujsConstraint):
    # pylint: disable=too-few-public-methods
    def __init__(self, owner=None):
        """
        a list length constraint
        """
        ujsConstraint.__init__(self, owner)
        self.value = None

    def __str__(self):
        return "length is " + self.value.val_as_str()


class ujsRange(ujsConstraint):
    def __init__(self, not_in=False, owner=None):
        """
        param not_in : if True, the constraint is reversed
        """
        ujsConstraint.__init__(self, owner)
        self.reversed = not_in
        self.values = []

    def range_table_as_str(self):
        s = Template(("#" * self.owner.nlevel) + """### $identifier Values

| Value | Description |
|-------|-------------|
""")
        type_id = self.owner_type
        if self.item_name is not None:
            type_id = type_id + " " + self.item_name

        d = {'identifier': type_id}
        s = s.substitute(d)

        for v in self.values:
            s = s + str(v) + "\n"
        return s

    def __str__(self):
        ref = self.owner_type
        if self.item_name is not None:
            ref = ref + '-' + self.item_name

        reversed_prefix = 'not ' if self.reversed else ''
        return reversed_prefix + 'in [Values](#' + ref + '-values)'


class ujsType():
    """
    base class for ujs type definitions
    """
    def __init__(self, owner=None):
        self.identifier = ''    # type identifier
        self.doc = ''           # documentation string
        self.extending = None   # type name of base list
        self.owner = owner      # a reference to the instance of the parser
        self.constraints = []   # a list of constraints
        self.type = ''          # base type identifier

    def append_constraint(self, constraint):
        """
        append a constraint and set this type as owner for building
        reference links
        """
        constraint.owner_type = self.identifier
        self.constraints.append(constraint)

    def constraints_as_str(self):
        """
        create a string to add in the constraints column
        """
        s = ''
        for c in self.constraints:
            s = s + str(c) + '<br>'
        return s

    def range_as_str(self):
        """
        return a values table section as string or None if no range 
        constraint was set
        """
        for c in self.constraints:
            if isinstance(c, ujsRange):
                return c.range_table_as_str()
        return None

 
class ujsPrimitiveType(ujsType):
    """
    a primitive type defintion consting of base type and constraints
    """
    def __init__(self, owner=None):
        ujsType.__init__(self, owner)

    def __str__(self):
        s = Template(("#" * self.owner.nlevel) + """## $identifier

$doc

| Identifier  | Type  | Constraints  |
|-------------|-------|--------------|
| $identifier | $type | $constraints |

""")
        if self.type in self.owner.reference_tab.keys():
            t = "[" + self.type + "](" + self.owner.reference_tab[self.type] + ")"
        else:
            t = self.type

        d = {'identifier': self.identifier,
             'type': t,
             'doc': self.doc,
             'constraints': self.constraints_as_str()}

        s = s.substitute(d)
        v_tab = self.range_as_str()
        if v_tab is not None:
            s = s + v_tab
        return s


class ujsHomogeneousMap(ujsType):
    """
    a map with a specific type for all keys and values
    """
    def __init__(self, owner=None):
        ujsType.__init__(self, owner)

        self.key_type = None
        self.value_type = None

    def __str__(self):
        self.key_type.identifier = self.identifier + ' Key'
        s = Template(("#" * self.owner.nlevel) + """## $identifier

$doc

**Map of**

| Identifier  | Key Type | Key Constraints | Value Type | Value Constraints |
|-------------|----------|-----------------|------------|-------------------|
| $identifier | $ktype   | $kconstraints   | $vtype     | $vconstraints     |

""")

        d = {'identifier': self.identifier,
             'ktype': self.owner.make_link(self.key_type.type),
             'vtype': self.owner.make_link(self.value_type.type),
             'doc': self.doc,
             'kconstraints': self.key_type.constraints_as_str(),
             'vconstraints': self.value_type.constraints_as_str()}

        s = s.substitute(d)

        v_tab = self.key_type.range_as_str()
        if v_tab is not None:
            s = s + v_tab

        v_tab = self.value_type.range_as_str()
        if v_tab is not None:
            s = s + "\n" + v_tab

        return s


class ujsHomogeneousList(ujsType):
    """
    a assign a type to all items of  a list
    """
    def __init__(self, owner=None):
        ujsType.__init__(self, owner)

    def __str__(self):
        s = Template(("#" * self.owner.nlevel) + """## $identifier

$doc

**List of**

| Identifier  | Type          | Constraints  |
|-------------|---------------|--------------|
| $identifier | list of $type | $constraints |

""")
        if self.type in self.owner.reference_tab.keys():
            t = "[" + self.type + "](" + self.owner.reference_tab[self.type] + ")"
        else:
            t = self.type

        d = {'identifier': self.identifier,
             'type': t,
             'doc': self.doc,
             'constraints': self.constraints_as_str()}

        s = s.substitute(d)
        v_tab = self.range_as_str()
        if v_tab is not None:
            s = s + v_tab
        return s


class ujsDataItem():
    """
    base class for ujs container item
    """

    # pylint: disable=too-many-instance-attributes

    def __init__(self, owner=None):
        self.type = ''           # type identifier
        self.of_type = None      # type of container elements
        self.doc = ''            # documentation string
        self.index = None        # index of the data item for reference
        self._field_name = None  # a self speaking name for the record entry
        self.owner = owner       # a reference to the parsing object

        self.owner_type = None   # the owner type name to create reference

        self.constraints = []    # a list of constraints

    @property
    def field_name(self):
        return self._field_name if self._field_name is not None else ''

    @field_name.setter
    def field_name(self, value):
        self._field_name = value    

    def constraints_as_str(self):
        """
        create a string to add in the constraints column
        """
        s = ''
        for c in self.constraints:
            if s != '':
                s = s + '<br>'
            s = s + str(c)
        return s        

    def value_table_as_string(self):
        for c in self.constraints:
            if isinstance(c, ujsRange):
                return c.range_table_as_str()
        return None

    def append_constraint(self, constraint):
        constraint.owner_type = self.owner_type
        constraint.item_name = self.field_name
        self.constraints.append(constraint)


class ujsListItem(ujsDataItem):
    """
    A list item
    """
    def __init__(self, owner=None):
        ujsDataItem.__init__(self, owner)
        self.list = None    # reference to parent list object

    def __str__(self):
        """
        return the data items as table row
        """
        s = Template("| $index | $name | $type | $constraints | $doc |")
        
        if self.type in self.owner.reference_tab.keys():
            t = "[" + self.type + "](" + self.owner.reference_tab[self.type] + ")"
        else:
            t = self.type

        d = {"index": self.index + self.list.index_offset,
             "name": self.field_name,
             "type": t,
             "constraints": self.constraints_as_str(),
             "doc": linefeed2br(self.doc)}

        return s.substitute(d)


class ujsVarTypeItem(ujsDataItem):
    """
    A variant type item
    """
    def __init__(self, owner=None):
        ujsDataItem.__init__(self, owner)

    def __str__(self):
        """
        return the data items as table row
        """
        s = Template("| $type | $constraints | $doc |")
        
        if self.type in self.owner.reference_tab.keys():
            t = "[" + self.type + "](" + self.owner.reference_tab[self.type] + ")"
        else:
            t = self.type

        d = {"type": t,
             "constraints": self.constraints_as_str(),
             "doc": linefeed2br(self.doc)}

        return s.substitute(d)        


class ujsMapItem(ujsDataItem):
    """
    A Map item
    """
    def __init__(self, owner=None):
        ujsDataItem.__init__(self, owner)
        self.key_value = None   # the map key value
        self.key_type = None    # forced type of the key value

    def __str__(self):
        """
        return the data items as table row
        """
        if self.key_type is None:
            s = Template("| $key | $type | $constraints | $doc |")
        elif self.key_value is None:
            s = Template("| $key_type | $type | $constraints | $doc |")
        else:
            s = Template("| $key as $key_type | $type | $constraints | $doc |")
        
        t = self.owner.make_link(self.type)

        if self.of_type is not None:
            of_t = self.owner.make_link(self.of_type)
            t = t + " of " + of_t

        d = {"key": self.key_value,
             "key_type": self.owner.make_link(self.key_type),
             "type": t,
             "constraints": self.constraints_as_str(),
             "doc": linefeed2br(self.doc)}

        return s.substitute(d)


class ujsComplexList(ujsType):
    """
    class to handle complex list types
    """
    def __init__(self, owner=None):
        ujsType.__init__(self, owner)
        self.record_items = []  # a list of record items
        self.index_offset = 0   # index offset for extended lists

    def new_item(self):
        item = ujsListItem(owner=self.owner)
        item.list = self
        item.index = len(self.record_items)
        item.owner_type = self.identifier
        self.record_items.append(item)
        self.owner.list_length_tab[self.identifier] = \
            self.owner.list_length_tab.get(self.identifier, 0) + 1
        return item

    def __str__(self):
        s = Template(("#" * self.owner.nlevel) + """## $identifier

$doc

$extends

**List of**

| Index | Key | Type | Values | Description |
|:-----:|-----|------|--------|-------------|
""")
        # extending
        if self.extending is None:
            lextends = ""
        else:
            if self.extending in self.owner.reference_tab.keys():
                lextends = "**Extends** [" + self.extending + "](" \
                    + self.owner.reference_tab[self.extending] + ")"
            else:
                lextends = "**Extends** " + self.extending

        d = {'identifier': self.identifier + "\n\n",
             'doc': self.doc,
             'extends': lextends}

        s = s.substitute(d)

        for item in self.record_items:
            s = s + str(item) + '\n'

        for item in self.record_items:
            vtab = item.value_table_as_string()
            if vtab is not None:
                s = s + "\n" + vtab
        return s


class ujsVariantType(ujsType):
    """
    class to handle a variant type
    """
    def __init__(self, owner=None):
        ujsType.__init__(self, owner)
        self.items = []  # a list of type items

    def new_item(self):
        i = ujsVarTypeItem(owner=self.owner)
        i.index = len(self.items)
        i.owner_type = self.identifier
        self.items.append(i)
        return i

    def __str__(self):
        s = Template(("#" * self.owner.nlevel) + """## $identifier$doc

**Variant of**

| Type | Values | Description |
|------|--------|-------------|
""")

        d = {'identifier': self.identifier + "\n\n",
             'doc': self.doc}

        s = s.substitute(d)

        for i in self.items:
            s = s + str(i) + '\n'

        for i in self.items:
            vtab = i.value_table_as_string()
            if vtab is not None:
                s = s + "\n" + vtab
        
        return s


class ujsComplexMap(ujsType):
    """
    class to handle complex map types
    """
    def __init__(self, owner=None):   
        ujsType.__init__(self, owner)
        self.map_items = []  # a list of map items

    def new_item(self):
        i = ujsMapItem(owner=self.owner)
        i.index = len(self.map_items)
        i.owner_type = self.identifier
        self.map_items.append(i)
        return i

    def __str__(self):
        s = Template(("#" * self.owner.nlevel) + """## $identifier

$doc

$extends

**Map of**

| Key | Value Type | Values | Description |
|-----|------------|--------|-------------|
""")
        # extending
        if self.extending is None:
            lextends = ""
        else:
            if self.extending in self.owner.reference_tab.keys():
                lextends = "**Extends** [" + self.extending + "](" \
                    + self.owner.reference_tab[self.extending] + ")"
            else:
                lextends = "**Extends** " + self.extending
            lextends = lextends
        
        d = {'identifier': self.identifier + "\n\n",
             'doc': self.doc,
             'extends': "\n\n" + lextends}

        s = s.substitute(d)

        for i in self.map_items:
            s = s + str(i) + '\n'

        return s


class ujsParse2md():
    def __init__(self):
        self.parser = parser.UjsParser(start="begin")
        self.reference_tab = {}     # <type identifier>:<reference>
        self.list_length_tab = {}   # <type identifier>:<list length>
        self.nlevel = 0             # nesting level for rendering

    def make_link(self, type_name):
        """
        create a link to a type defintion for a given type name

        paramters:
            type_name : name of the data type

        return:
            markdown link as string or the type name
        """
        if type_name in self.reference_tab.keys():
            r = "[" + type_name + "](" + self.reference_tab[type_name] + ")"
        else:
            r = type_name
        return r

    def parse_text2md(self, atext, nlevel=0):
        """
        Parse a filename and return a list of type definition objects

        parameter:
            atext  : source text
            nlevel : nesting level - add nlevel*"#" to headlines

        Return:
            [string] Markdown formatted string
        """
        self.nlevel = nlevel
        type_defs = []
        ast = self.parser.parse_string(atext)

        for d in ast.children:
            if isinstance(d, Tree):
                if d.data == "primitive_type":
                    ujs_type = self.parse_type(ujsPrimitiveType(owner=self),
                                               d.children)    
                    type_defs.append(ujs_type)                
                elif d.data == "complex_list":
                    ujs_type = self.parse_type(ujsComplexList(owner=self),
                                               d.children)
                    type_defs.append(ujs_type)
                elif d.data == "complex_map":
                    ujs_type = self.parse_type(ujsComplexMap(owner=self),
                                               d.children)
                    type_defs.append(ujs_type)
                elif d.data == "module_pops":
                    ujs_type = self.parse_type(ujsModuleProps(), d.children)
                    type_defs.append(ujs_type) 
                elif d.data == "variant_type":
                    ujs_type = self.parse_type(ujsVariantType(owner=self),
                                               d.children)
                    type_defs.append(ujs_type) 
                elif d.data == "homogeneous_list":
                    ujs_type = self.parse_type(ujsHomogeneousList(owner=self),
                                               d.children)
                    type_defs.append(ujs_type) 
                elif d.data == "homogeneous_map":
                    ujs_type = self.parse_type(ujsHomogeneousMap(owner=self),
                                               d.children)
                    type_defs.append(ujs_type) 

        md = ""
        for t in type_defs:
            if md != "":
                md = md + "\n" + str(t)
            else:
                md = md + str(t)                    
        
        return md

    def parse_file2md(self, afilename, nlevel=0):
        """
        Parse a filename and return a list of type definition objects

        Parameter:
            afilename : full path of an *.ujs file
            nlevel : nesting level - add nlevel*"#" to headlines

        Return:
            [string] Markdown formatted string
        """
        with open(afilename) as f:
            md = self.parse_text2md(f.read(), nlevel)
            
        return md
        
    def print_pretty(self, afilename):
        """
        print the parse tree for reference
        """
        ast = self.parser.parse_file(afilename)
        print(ast.pretty())

    def get_range_item(self, aconstraint, items):
        """
        get a range items from the parse tree and return it as an object
        """
        # pylint: disable=no-self-use
        for d in items:
            if isinstance(d, Tree):
                if d.data == 'value':
                    if aconstraint.type == 'between':
                        if aconstraint.low_value is None: 
                            aconstraint.low_value = d.children[0].value
                        else:
                            aconstraint.high_value = d.children[0].value                                                      
                    else:
                        aconstraint.value = d.children[0].value
                if d.data == 'chars':
                    aconstraint.value = d.children[0].value
                elif d.data == 'documentation':
                    aconstraint.doc = d.children[0].value
                
        return aconstraint

    def get_map_key(self, amapitem, items):
        """
        parse a map key
        """
        for d in items:
            if isinstance(d, Tree):
                if d.data == 'key':
                    amapitem.key_value = d.children[0].value
                elif d.data == 'data_type':
                    self.get_map_key(amapitem, d.children)
                elif d.data == 'atomic_type':
                    amapitem.key_type = d.children[0].data
                elif d.data == 'custom_type':
                    amapitem.key_type = d.children[0].value

    def get_constraint(self, aconstraint, items):
        # pylint: disable=too-many-branches
        for d in items:
            if isinstance(d, Tree):
                if d.data == 'not_null':
                    aconstraint = self.get_constraint(ujsNotNull(owner=self),
                                                      d.children)
                if d.data == 'of_length':
                    aconstraint = self.get_constraint(ujsLength(owner=self),
                                                      d.children)
                elif d.data == 'in':
                    aconstraint = self.get_constraint(ujsRange(False,
                                                               owner=self),
                                                      d.children)
                elif d.data == 'default':
                    aconstraint.default_val = d.children[0].value
                elif d.data in ['range', 'words', 'length']:
                    aconstraint = self.get_constraint(aconstraint, d.children)
                elif d.data == 'equals':
                    avalue = self.get_range_item(ujsRangeValue(), d.children)
                    avalue.type = 'equals'
                    if isinstance(aconstraint, ujsLength):
                        aconstraint.value = avalue
                    else:
                        aconstraint.values.append(avalue) 
                elif d.data == 'above':
                    avalue = self.get_range_item(ujsRangeValue('above'),
                                                 d.children)
                    if isinstance(aconstraint, ujsLength):
                        aconstraint.value = avalue
                    else:
                        aconstraint.values.append(avalue) 
                elif d.data == 'below':
                    avalue = self.get_range_item(ujsRangeValue('below'),
                                                 d.children)
                    if isinstance(aconstraint, ujsLength):
                        aconstraint.value = avalue
                    else:
                        aconstraint.values.append(avalue) 
                elif d.data == 'between':
                    avalue = self.get_range_item(ujsRangeValue('between'),
                                                 d.children)
                    if isinstance(aconstraint, ujsLength):
                        aconstraint.value = avalue
                    else:
                        aconstraint.values.append(avalue) 
                elif d.data == 'word':
                    avalue = self.get_range_item(ujsRangeValue('word'),
                                                 d.children)
                    aconstraint.values.append(avalue)
                    
        return aconstraint  

    def parse_type(self, atype, items):
        """
            parse tree and build data types

            Parameter:
                atype : the current data type
                items : subtree from parser
        """
        # pylint: disable=too-many-nested-blocks
        # pylint: disable=too-many-branches
        # pylint: disable=too-many-statements
        for d in items:
            if isinstance(d, Tree):
                if d.data == 'identifier':
                    atype.identifier = d.children[0].value
                    self.reference_tab[atype.identifier] = "#" + atype.identifier 
                elif d.data in ['data_type']:
                    if isinstance(atype, ujsHomogeneousMap):
                        if atype.key_type is None:
                            atype.key_type = self.parse_type(ujsType(atype),
                                                             d.children)
                            atype.key_type.identifier = atype.identifier + " Keys"
                        elif atype.value_type is None:
                            atype.value_type = self.parse_type(ujsType(atype), 
                                                               d.children)
                            atype.value_type.identifier = atype.identifier + " Item"
                    else:    
                        self.parse_type(atype, d.children)
                elif d.data == 'custom_type':
                    if isinstance(atype, (ujsComplexList, ujsComplexMap)):
                        atype.extending = d.children[0].value
                        atype.index_offset = self.list_length_tab.get(d.children[0].value, 0)
                        self.list_length_tab[atype.identifier] = \
                            self.list_length_tab.get(atype.identifier, 0) +\
                            atype.index_offset
                    else:
                        if atype.type == 'list':
                            atype.of_type = d.children[0].value
                        else:
                            atype.type = d.children[0].value
                elif d.data == 'atomic_type':
                    if atype.type == 'list':
                        atype.of_type = d.children[0].data
                    else:
                        atype.type = d.children[0].data
                elif d.data == 'list':
                    if not isinstance(atype, ujsHomogeneousList):
                        atype.type = d.data
                elif d.data == 'variant':
                    atype.type = d.data
                elif d.data == 'documentation':
                    if d.children[0].type == 'LONG_STRING':
                        atype.doc = d.children[0].value[3:-3]
                    elif d.children[0].type == 'STRING': 
                        atype.doc = d.children[0].value[1:-1]
                elif d.data == 'constraint':
                    lconstraint = self.get_constraint(None, d.children)
                    if lconstraint is not None:
                        if isinstance(atype, ujsHomogeneousMap):
                            if atype.value_type is None:
                                atype.key_type.append_constraint(lconstraint)
                            else:
                                atype.value_type.append_constraint(lconstraint)
                        else:    
                            atype.append_constraint(lconstraint)
                elif d.data == 'item':
                    if isinstance(atype, ujsMapItem):
                        self.parse_type(atype, d.children)
                    else:
                        self.parse_type(atype.new_item(), d.children)
                elif d.data == 'key_value_definition':
                    self.parse_type(atype.new_item(), d.children)
                elif d.data == 'key':
                    self.get_map_key(atype, d.children)
                elif d.data == 'field_name':
                    atype.field_name = d.children[0].value
                elif d.data == 'extending':
                    self.parse_type(atype, d.children)

        return atype    
