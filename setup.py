#
# Copyright (c) 2018-present, wobe-systems GmbH
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.
#

import setuptools

with open("doc/markdown/ujo-schemes.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="UJOScheme",
    version="0.0.3-1",
    author="Maik Wojcieszak",
    author_email="mw@wobew-systems.com",
    description="UJO Schemes is an easy to read and easy " +
    "to write language to define UJO data structures.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://www.industrial-devops.org",
    packages=setuptools.find_packages(),
    include_package_data=True,
    package_data={
        "UJOScheme": ["grammar/*.lark"],
    },
    data_files=[
        ("", ["LICENSE"])
    ],
    install_requires=[
        "lark-parser",
    ],
    license="MIT",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    scripts=["bin/ujs2md.py"]
)
