# UJO Schemes

Documentation of UJO Schemes and a Python implementation of a parser and related tools.

## Install Requirements

There a a number of packages required to be installes with python.

``` 
pip install -r requirements.txt
```

## Convert UJO Schemes to markdown documentation

UJO Schemes files can be converted into a markdown documentation ```ujs2md.py```.

__Usage:__

```
usage: ujs2md.py [-o <output>] <source>

positional arguments:
  source                path to ujo scheme file or folder
                        if providing a folder by default all files will be processed
                        you might need to provide an extension by using the "-ext/--extension" option

optional arguments:
  -h, --help            show this help message and exit
  -o <output>, --output <output>
                        output path for markdown
  -ext <extension>, --extension <extension>
                        filter files by extension if providing a folder as <ujs_file>
                        example: ".ujs"
                        defaults to ".ujs"
```

__Example:__

```
python .\ujs2md.py -o testoutput -ext .ujs .\examples\ujs2md
```

# Generate Documentation

For code documentation [Sphinx]() is used together with the 
[Napoleon](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/index.html)
extension.

On Windows you can generate the documentation with:

```
./doc/make.bat html
```

For Linux there is a `Makefile` prepared, but not yet tested.

# Code Quality

A variety of tools can assist in writing quality code. This chapter explains
which are used and how tu run them.

## pylint

Linting is performed with [pylint](https://www.pylint.org). To define the
intended checks `.pylintrc` is used to configure linting for this project.

A default configuration file was generated with

```
pylint --generate-rcfile
```

The generated file was than modified to exclude some tests.

Running pylint for the python code in this project the following commands are
used:

```
pylint --rcfile=.pylintrc .\UJOScheme\
pylint --rcfile=.pylintrc .\bin
```

## flake8

To make sure the PEP8 standard is applied to the code flake8 can be added to 
the static tests.

For this project we exclude various errors, warnings and notifications because
they do not make sense at this time. This may change while refactoring is
considered.

You can run flake 8 with:

```
flake8
```

It finds all the python files in this project.
The configuration for this project is read from `.flake8` in  the project
root directory.