# module name

## new_type

test document string
    eine neue Zeile

|Key          | Type        | Constraints    |
|-------------|-------------|----------------|
| new_type |        | not null default 5<br>in [Values](#new_type-values)<br>   |

### new_type Values

|Value|Description|
|-----|-----------|
| 5.7 | "keine Ahnung was das sein soll!" |
| 3.14 | "Pi" |
```
new_type = float64 
    : not null default 5
    : in (
        5.7     : doc "keine Ahnung was das sein soll!",
        3.14    : doc "Pi") 
    : doc """test document string
    eine neue Zeile""";
```

## new_str

| Key | Type| Values | Description |
|-----|-----|--------|-------------|
|new_str|cstring|"Test"<br>"Blah"||

```
new_str = cstring
    : in (
        "Test",
        "Blah"
    );
```

## header

This is a record

| Key | Type| Values | Description |
|-----|-----|--------|-------------|
|CreationTime|timestamp||Creation time of the message|
|SequenceNumber|int64||sequence number to order the messages|
|Status|int16|not null<br>[Status Values](#status-values)|Processing status|
|Message|string||An error message|
|Values|list||a list with some values|

### Status Values

|Value|Description|
|-----|-----------|
|0|Ok|
|1|Warning|
|2|Error|
|3|Critical|

```
header = list [
    timestamp
        : name CreationTime
        : doc "Creation time of the message",
    int64
        : name SequenceNumber
        : doc "sequence number to order the messages",
    int16
        : name Status
        : in (
                0 : doc "Ok",
                1 : doc "Warning",
                2 : doc "Error",
                3 : doc "Critical"
            )
        : not null
        : doc "Processing status",
    string
        : name Message
        : doc "An error message",
    list
        : name Values
        : doc "a list with some values"
] : doc "This is a record";
```

## status_type

This is a record

| Key | Type| Values | Description |
|-----|-----|--------|-------------|
|status_type|int16|0 : Ok<br>1 : Warning<br>2 : Error<br>3 : Critical<br>not null|Processing status|

## refheader

This is a record

| Key | Type| Values | Description |
|-----|-----|--------|-------------|
|CreationTime|timestamp||Creation time of the message|
|SequenceNumber|int64||sequence number to order the messages|
|Status|[status_type](#status_type)||Processing status|
|Message|string||An error message|
|Values|list||a list with some values|


```
status_type = int16
        : in (
                0 : doc "Ok",
                1 : doc "Warning",
                2 : doc "Error",
                3 : doc "Critical"
            )
        : not null
        : doc "Processing status";


refheader = list [
    timestamp
        : name CreationTime
        : doc "Creation time of the message",
    int64
        : name SequenceNumber
        : doc "sequence number to order the messages",
    status_type
        : name Status
        : doc "Processing status",
    string
        : name Message
        : doc "An error message",
    list
        : name Values
        : doc "a list with some values"
] : doc "This is a record";
```

## mapType

map defintion

| Key | Value| Values | Description |
|-----|-----|--------|-------------|
|3.14 as float32|list|||
|"temperature"|cstring||another doc string|

```
mapType = map { 3.14 as float32 | list, 
                "temperature" | cstring : doc "another doc string" }
: doc "map defintion";
```

## keyTypeMap

| Key | Value| Values | Description |
|-----|-----|--------|-------------|
|int64|variant||my type definition|

```
keyTypeMap = map of int64 | variant : doc "my type defintion";
```

## extMapType

map defintion

#### Extends [keyTypeMap](#keyTypeMap)

| Key | Value| Values | Description |
|-----|-----|--------|-------------|
|5|list|||
|"test"|cstring||another doc string

```
extMapType = map extends mapType { 5 | list, 
                "test" | cstring : doc "another doc string" }
: doc "map defintion";
```

## mytype

| Key | Type| Values | Description |
|-----|-----|--------|-------------|
|mytype|string|-3 .. 3<br>5 ..|This field represents a|

```
mytype = string   
    : in (-3 .. 3, 5 ..) 
    : doc "This field represents a"; 
```

## list_based

multiline
a simple docstring

#### Extends [atest](#atest)

| Key | Type| Values | Description |
|-----|-----|--------|-------------|
|Validfield|[mytype](#mytype)|||
|BoolField|boolean|not null||

```
list_based = list extends atest [ 
  mytype   : name ValidField,   
  boolean  : name BoolField 
    : not null]
: doc """multiline
a simple docstring""";
```

## intlist

a typed list with minimum length

| Key | Type| Values | Description |
|-----|-----|--------|-------------|
|intlist|list of int64|length(10..)|a typed list with minimum length|

```
intlist = list of int64 
  : length(10 ..) 
  : doc "a typed list with minimum length";
```

## based

a record with type defs

| Key | Type| Values | Description |
|-----|-----|--------|-------------|
||int64|10.0..100.0|This field represents a|
||boolean|not null|true or false|

```
based = list [
    int64 : in (10.0..100.0)   : doc "This field represents a",
    boolean : not null : doc "true or false",
]:doc "a record with type defs";
```
