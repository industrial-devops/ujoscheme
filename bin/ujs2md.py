#
# Copyright (c) 2018-present, wobe-systems GmbH
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.
#

import os.path
from argparse import ArgumentParser, RawTextHelpFormatter
from textwrap import dedent
import re

from UJOScheme import parser
from UJOScheme import parse2md


def parse_arguments():
    lparser = ArgumentParser(usage="ujs2md.py [-o <output>] <source>",
                             formatter_class=RawTextHelpFormatter)

    lparser.add_argument("source",
                         metavar="source",
                         help=dedent("""\
                               path to ujo scheme file or folder
                               if providing a folder by default all files will be processed
                               you might need to provide an extension by using the "-ext/--extension" option                            
                               """))

    lparser.add_argument("-o", "--output",
                         dest="output",
                         metavar="<output>",
                         default="",
                         help=dedent("""\
                             output path for markdown
                             """))

    lparser.add_argument("-ext", "--extension",
                         dest="extension",
                         metavar="<extension>",
                         default="",
                         help=dedent("""\
                             filter files by extension if providing a folder as <ujs_file> 
                             example: ".ujs"                              
                             defaults to ".ujs"
                             """))

    return lparser.parse_args()


def parse_text2markdown(aparser, atext, alevel=0):
    """
    Create a markdown formatted string from a source text.

    Parameter:
        aparser :[ujsParse2md] instance of a parser that handles the reference tables
        atext   :[string] ujs source as text

    Return:
        [string] markdown formatted string
    """
    return aparser.parse_text2md(atext, alevel)


def parse_file2markdown(aparser, afilename, alevel=0):
    """
    Create a markdown formatted string from a ujs file.

    Parameter:
        aparser :[ujsParse2md] instance of a parser that handles the reference tables
        afilename [string] complete path of  a ujs file to convert

    Return:
        [string] markdown formatted string
    """
    with open(afilename) as f:
        md = parse_text2markdown(aparser, f.read(), alevel) 

    return md


def get_nesting_level(aline):
    """
    Check if a line is a headline and return the number of '#"

    Parameter:
        aline : a markdown line

    Return:
        int : nesting level
    """
    match = re.search(r"^\s*#*", aline)
    found = match.group(0)
    level = 0
    for line_char in found:
        if line_char == '#':
            level = level + 1
    return level


def md2md(aparser, afilename):
    """
    Process a markdown file and search for code blocks with ujs code.
    Each code block is translated into markdown and the result is returned and combined
    with the rest of the markdown.

    Parameter:
        aparser :[ujsParse2md] instance of a parser that handles the reference tables
        afilename : [string] full path of source file

    Return:
        [string] processed markdown
    """
    print("*** process markdown file ", afilename)
    with open(afilename) as f:
        lines = f.readlines()

    ujo_block = False
    new_md = ""
    ujo_src = ""
    nlevel = 0   # nesting level

    for l in lines:
        line_level = get_nesting_level(l)
        if line_level > 0:
            nlevel = line_level - 1
            print("Level: ", nlevel)

        if re.match(r"^\s*```ujo\s*$", l):
            print("***start ujo found ***")
            ujo_block = True
            ujo_src = ""
            continue
        elif re.match(r"^\s*```\s*$", l) and ujo_block:
            print("***end ujo found ***")
            ujo_block = False
            print(ujo_src)
            line_number = 1
            for sline in ujo_src.splitlines():
                print(line_number, " ", sline)
                line_number = line_number + 1

            md = parse_text2markdown(aparser, ujo_src, alevel=nlevel)
            new_md = new_md + md
            continue

        if ujo_block:
            ujo_src = ujo_src + l
        else: 
            new_md = new_md + l
        
    return new_md


def main():
    arguments = parse_arguments()
    print(arguments)

    if not arguments.extension:
        file_ext = ".ujs"
    else:
        file_ext = arguments.extension 
    ujs_files = parser.to_filename_tuple(arguments.source, file_ext)

    md_parser = parse2md.ujsParse2md()

    for filename in ujs_files:
        print(f"--- {filename} ---")

        if os.path.splitext(filename)[-1] == ".md":
            md = md2md(md_parser, filename)
        else:
            md = parse_file2markdown(md_parser, filename)

        md_parser.print_pretty(filename)

        # generate output filename
        out_file = os.path.splitext(os.path.basename(filename))[0] + ".md"

        if arguments.output:
            out_path = arguments.output
            if not os.path.exists(out_path):
                os.makedirs(out_path)
        else:                
            out_path = os.path.dirname(filename)
        out_file = os.path.join(out_path, out_file)
            
        print("write md to:", out_file)
        file = open(out_file, 'w')
        file.write(md)
        file.close()


# run the main function
if __name__ == '__main__':
    main()
